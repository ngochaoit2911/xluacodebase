using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using UnityEditor;
using System.IO;
using DG.DemiEditor;
using Editor;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using XLua;

public static class LuaApiExporter
{
    public static string DirectoryPath = "Assets/UnityLuaAPI/Editor";
    static void Gen()
    {
        string path = "Assets/EmmyApi/Editor/";
        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }

        Directory.CreateDirectory(path);
        //UnityEngine
        GenAssembly(path, null);
        //GenAssembly("UnityEngine.UI", path);
        GenCustom(path);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public static void GenAll(CSharpTypeGenerateLuaData data)
    {
        if (Directory.Exists(DirectoryPath))
        {
            Directory.Delete(DirectoryPath, true);
        }

        Directory.CreateDirectory(DirectoryPath);
        //UnityEngine
        GenAssembly(DirectoryPath, data);
        //GenAssembly("UnityEngine.UI", path);
        GenCustom(DirectoryPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    public static void GenAType(string type)
    {
        var file = $"{DirectoryPath}/{type}.lua";
        if (File.Exists(file))
        {
            File.Delete(file);
        }
        GenType(ByName(type), false, DirectoryPath);
    }
    public static void GenAssembly(string path, CSharpTypeGenerateLuaData data)
    {
        OnGetNoUseList(out var excludeList);

        foreach (var t in data.exportTypes)
        {
            var m = ByName(t);
            Debug.Log(m);
            if (m != null)
            {
                if (filterType(m, excludeList))
                {
                    GenType(m, false, path);
                } 
            }

        }
    }

    public static List<Type> GetAllTypes()
    {
        var temps = new List<Type>();
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Reverse())
        {
            if (!assembly.IsDynamic)
            {
                foreach (var type in assembly.ExportedTypes)
                {
                    temps.Add(type);
                }
            }
        }

        return temps;
    }
    
    public static Type ByName(string name)
    {
        foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().Reverse())
        {
            var tt = assembly.GetType(name);
            
            if (tt != null)
            {
                return tt;
            }
        }

        return null;
    }
 

    static bool filterType(Type t, List<string> noUseList)
    {
        // check type in uselist
        string fullName = t.FullName;

        // check type not in nouselist
        foreach (string str in noUseList)
        {
            if (fullName != null && fullName.Contains(str))
            {
                return false;
            }
        }

        return true;
    }
    // black list if white list not given
    public static void OnGetNoUseList(out List<string> list)
    {
        list = new List<string>
        {
            "HideInInspector",
            "ExecuteInEditMode",
            "AddComponentMenu",
            "ContextMenu",
            "RequireComponent",
            "DisallowMultipleComponent",
            "SerializeField",
            "AssemblyIsEditorAssembly",
            "Attribute",
            "Types",
            "UnitySurrogateSelector",
            "TrackedReference",
            "TypeInferenceRules",
            "FFTWindow",
            "RPC",
            "Network",
            "MasterServer",
            "BitStream",
            "HostData",
            "ConnectionTesterStatus",
            "GUI",
            "EventType",
            "EventModifiers",
            "FontStyle",
            "TextAlignment",
            "TextEditor",
            "TextEditorDblClickSnapping",
            "TextGenerator",
            "TextClipping",
            "Gizmos",
            "ADBannerView",
            "ADInterstitialAd",
            "Android",
            "Tizen",
            "jvalue",
            "iPhone",
            "iOS",
            "CalendarIdentifier",
            "CalendarUnit",
            "CalendarUnit",
            "ClusterInput",
            "FullScreenMovieControlMode",
            "FullScreenMovieScalingMode",
            "Handheld",
            "LocalNotification",
            "NotificationServices",
            "RemoteNotificationType",
            "RemoteNotification",
            "SamsungTV",
            "TextureCompressionQuality",
            "TouchScreenKeyboardType",
            "TouchScreenKeyboard",
            "MovieTexture",
            "UnityEngineInternal",
            "Terrain",
            "Tree",
            "SplatPrototype",
            "DetailPrototype",
            "DetailRenderMode",
            "MeshSubsetCombineUtility",
            "AOT",
            "Social",
            "Enumerator",
            "SendMouseEvents",
            "Cursor",
            "Flash",
            "ActionScript",
            "OnRequestRebuild",
            "Ping",
            "ShaderVariantCollection",
            "SimpleJson.Reflection",
            "CoroutineTween",
            "GraphicRebuildTracker",
            "Advertisements",
            "UnityEditor",
            "WSA",
            "EventProvider",
            "Apple",
            "ClusterInput",
        };
    }

    public static void GenCustom(string path)
    {
        Type[] types = Assembly.Load("Assembly-CSharp").GetTypes();
        foreach (Type t in types)
        {
            if (t.IsDefined(typeof(LuaCallCSharpAttribute), false))
            {
                GenType(t, true, path);
            }
        }
    }

    public static void GenType(Type t, bool custom, string path)
    {
        if (!CheckType(t, custom))
            return;
        //TODO System.MulticastDelegate
        var sb = new StringBuilder();
        if (!CheckType(t.BaseType, custom))
            sb.AppendFormat("---@class {0}\n", t.Name);
        else
            sb.AppendFormat("---@class {0} : {1}\n", t.Name, t.BaseType.Name);
        GenTypeField(t, sb);
        sb.AppendFormat("local {0}={{ }}\n", t.Name);

        GenTypeMehod(t, sb);

        if (t.Namespace != "")
        {
            sb.AppendFormat("{0}.{1} = {2}", t.Namespace, t.Name, t.Name);
        }
        else
        {
            sb.AppendFormat("{0} = {1}", t.Name, t.Name);

        }

        File.WriteAllText($"{path}/{t.FullName}.lua", sb.ToString(), Encoding.UTF8);
    }

    static bool CheckType(Type t, bool custom)
    {
        if (t == null)
            return false;
        if (t == typeof(System.Object))
            return false;
        if (t.IsGenericTypeDefinition)
            return false;
        if (t.IsDefined(typeof(ObsoleteAttribute), false))
            return false;
        if (t == typeof(YieldInstruction))
            return false;
        if (t == typeof(Coroutine))
            return false;
        if (t.IsNested)
            return false;
        if (custom && !t.IsDefined(typeof(LuaCallCSharpAttribute), false))
            return false;
        return true;
    }

    public static void GenTypeField(Type t, StringBuilder sb)
    {
        FieldInfo[] fields = t.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance |
                                         BindingFlags.DeclaredOnly);
        foreach (var field in fields)
        {
            if (field.IsDefined(typeof(DoNotGenAttribute), false))
                continue;
            sb.AppendFormat("---@field public {0} {1}\n", field.Name, GetLuaType(field.FieldType));
        }

        PropertyInfo[] properties = t.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance |
                                                    BindingFlags.DeclaredOnly);
        foreach (var pro in properties)
        {
            if (pro.IsDefined(typeof(DoNotGenAttribute), false))
                continue;
            sb.AppendFormat("---@field public {0} {1}\n", pro.Name, GetLuaType(pro.PropertyType));
        }
    }

    public static void GenTypeMehod(Type t, StringBuilder sb)
    {
        MethodInfo[] methods = t.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance |
                                            BindingFlags.DeclaredOnly);
        foreach (var method in methods)
        {
            if (method.IsGenericMethod)
                continue;
            if (method.IsDefined(typeof(DoNotGenAttribute), false))
                continue;
            if (method.Name.StartsWith("get_") || method.Name.StartsWith("set_"))
                continue;
            sb.AppendLine("---@public");
            var paramstr = new StringBuilder();
            foreach (var param in method.GetParameters())
            {
                sb.AppendFormat("---@param {0} {1}\n", param.Name, GetLuaType(param.ParameterType));
                if (paramstr.Length != 0)
                {
                    paramstr.Append(", ");
                }

                paramstr.Append(param.Name);
            }

            sb.AppendFormat("---@return {0}\n", method.ReturnType == null ? "void" : GetLuaType(method.ReturnType));
            if (method.IsStatic)
            {
                sb.AppendFormat("function {0}.{1}({2}) end\n", t.Name, method.Name, paramstr);
            }
            else
            {
                sb.AppendFormat("function {0}:{1}({2}) end\n", t.Name, method.Name, paramstr);
            }
        }
    }

    static string GetLuaType(Type t)
    {
        if (t.IsEnum
            //|| t == typeof(ulong)
            //|| t == typeof(long)
            //|| t == typeof(int)
            //|| t == typeof(uint)
            //|| t == typeof(float)
            || t == typeof(double)
            //|| t == typeof(byte)
            //|| t == typeof(ushort)
            //|| t == typeof(short)
        )
            return "number";
        if (t == typeof(bool))
            return "boolean";
        if (t == typeof(string))
            return "string";
        if (t == typeof(void))
            return "void";
        if (IsUnityEvent(t))
            return "UnityEvent";
        return t.Name;
    }

    public static bool IsUnityEvent(Type type)
    {
        return type.IsSubclassOf(typeof(UnityEvent)) || type == typeof(UnityEvent) ||
               type.IsSubclassOf(typeof(UnityEvent<bool>)) || type.IsSubclassOf(typeof(UnityEvent<string, string>));
    }
}