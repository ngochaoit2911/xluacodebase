// Assembly-CSharp-Editor

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class CSharpTypeExportWindow : EditorWindow
    {
        [MenuItem("Tools/ExportCSharpType")]
        private static void ShowWindow()
        {
            var window = GetWindow<CSharpTypeExportWindow>();
            window.titleContent = new GUIContent("ExportCSharpType");
            window.Show();
            window.types = LuaApiExporter.GetAllTypes().Select(s => s.FullName).ToArray();
            window.searchTypes = window.types;
        }

        private CSharpTypeGenerateLuaData data;
        private string type;
        private string[] types;
        private string[] searchTypes;
        private int curSelectType;

        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            data = (CSharpTypeGenerateLuaData) EditorGUILayout.ObjectField("ExportData",data, typeof(CSharpTypeGenerateLuaData), true);
            GUILayout.EndHorizontal();
            if (data)
            {
                GUILayout.BeginHorizontal();
                type = EditorGUILayout.TextField("Type", type);

                if (GUI.changed)
                {
                    GetSearchType(type.Trim());
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                curSelectType = EditorGUILayout.Popup("All Types", curSelectType, searchTypes);
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                
                if (GUILayout.Button("Gen Type"))
                {
                    var m = LuaApiExporter.ByName(searchTypes[curSelectType]);
                    if (m != null)
                    {
                        if (!data.exportTypes.Contains(searchTypes[curSelectType]))
                        {
                            Debug.Log(searchTypes[curSelectType]);
                            data.exportTypes.Add(searchTypes[curSelectType]);
                            LuaApiExporter.GenAType(type);
                            EditorUtility.SetDirty(data);
                            AssetDatabase.SaveAssets();
                            AssetDatabase.Refresh();
                        }
                    }
                    else
                    {
                        Debug.LogError("Type Not Exist!");
                    }
                    
                }
                
                if (GUILayout.Button("Gen All"))
                {
                    LuaApiExporter.GenAll(data);
                }
                
                if (GUILayout.Button("GenerateFirst"))
                {
                    data.GenerateFirst();
                    EditorUtility.SetDirty(data);
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Clear"))
                {
                    if (Directory.Exists(LuaApiExporter.DirectoryPath))
                    {
                        Directory.Delete(LuaApiExporter.DirectoryPath);
                    }
                }
                GUILayout.EndHorizontal();
            }
        }

        private void GetSearchType(string s)
        {
            if(types == null)
                return;
            searchTypes = types.Where(m => m.Split('.').Last().ToLower().Contains(s.ToLower())).OrderBy(m => m.Length).ToArray();
        }

    }
}