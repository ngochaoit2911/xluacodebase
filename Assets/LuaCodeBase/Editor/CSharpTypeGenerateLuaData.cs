// Assembly-CSharp-Editor

using System;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Component = UnityEngine.Component;

namespace Editor
{
    [CreateAssetMenu(fileName = "LuaExportData", menuName = "Data/LuaExportData", order = 0)]
    public class CSharpTypeGenerateLuaData : ScriptableObject
    {
        public List<string> exportTypes;

        public void AddFromType(Type t)
        {
            exportTypes.Add(t.FullName);
        }

        public void AddFromListType(List<Type> t)
        {
            foreach (var type in t)
            {
                exportTypes.Add(type.FullName);
            }
        }

        [Button]
        public void GenerateFirst()
        {
            exportTypes = new List<string>();
            List<Type> useTypes = new List<Type>()
                {
                    typeof(Type),
                    typeof(System.Object),
                    typeof(UnityEngine.Object),
                    typeof(Vector2),
                    typeof(Vector3),
                    typeof(Vector4),
                    typeof(Quaternion),
                    typeof(Color),
                    typeof(Time),
                    typeof(GameObject),
                    typeof(Component),
                    typeof(Behaviour),
                    typeof(Transform),
                    typeof(Resources),
                    typeof(TextAsset),
                    typeof(Keyframe),
                    typeof(MonoBehaviour),
                    typeof(ParticleSystem),
                    typeof(Input),
                    typeof(KeyCode),
                    typeof(Mathf),
                    typeof(List<int>),
                    typeof(Action<string>),
                    typeof(Debug),
                    typeof(Button),
                    typeof(Toggle),
                    typeof(TextMeshProUGUI),
                    typeof(TMP_InputField),
                    typeof(bool),
                    typeof(Int32),
                    typeof(Scene),
                    typeof(Single),
                    typeof(Button.ButtonClickedEvent),
                    typeof(UnityEvent),
                    typeof(UnityEvent<object>),
                    typeof(PlayerPrefs),
                    typeof(WaitForSeconds),
                    typeof(WaitUntil),
                    typeof(WaitForEndOfFrame),
                    typeof(WaitForSecondsRealtime),
                };
            AddFromListType(useTypes);
        }
    }
}