﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using libx;
using LuaFramework;
using UnityEngine;
using UnityEngine.Events;
using XLua;
using IAsyncResult = LuaFramework.IAsyncResult;

public static class LuaGenSettings
{
    [LuaCallCSharp] 
    public static List<Type> lua_call_cs_list = new List<Type>()
    {
        typeof(Executors),
        typeof(Preferences),
        typeof(AsyncResult),
        typeof(ObservableDictionary<object, object>),
        typeof(ObservableList<object>),
        typeof(NotifyCollectionChangedEventArgs),
        typeof(NotifyCollectionChangedEventHandler),
         typeof(Type),
        typeof(CoroutineAwaiterExtensions),
         typeof(UnityAction<bool>),
         typeof(UnityAction<object>),
         typeof(UnityAction),
         typeof(InterceptableEnumerator),
         typeof(System.Object),
         typeof(UnityEngine.Object),
         typeof(Vector2),
         typeof(Vector3),
         typeof(Vector4),
         typeof(Quaternion),
         typeof(Color),
         typeof(Ray),
         typeof(Bounds),
         typeof(Ray2D),
         typeof(Time),
         typeof(GameObject),
         typeof(Component),
         typeof(Behaviour),
         typeof(Transform),
         typeof(Resources),
         typeof(TextAsset),
         typeof(Keyframe),
         typeof(AnimationCurve),
         typeof(AnimationClip),
         typeof(MonoBehaviour),
         typeof(ParticleSystem),
         typeof(SkinnedMeshRenderer),
         typeof(Renderer),
         typeof(WWW),
         typeof(Mathf),
         typeof(System.Collections.Generic.List<int>),
         typeof(Action<string>),
         typeof(UnityEngine.Debug),
         typeof(IAwaiter),
         typeof(IAwaiter<object>),
         typeof(IAwaiter<int>),
         typeof(ILuaTask<int>),
         typeof(WaitForSecondsRealtime),
         typeof(WaitForSeconds),
         typeof(Path),
         typeof(Assets),
         typeof(AssetRequest),
    };

    [CSharpCallLua] 
    public static List<Type> cs_call_lua_list = new List<Type>()
    {
        typeof(IEnumerator),
        typeof(Action),
        typeof(Action<LuaTable>),
        typeof(Action<MonoBehaviour>),
        //typeof(Func<MonoBehaviour, ILuaTask>),
        typeof(Action<float>),
        typeof(Action<int>),
        typeof(Action<string>),
        typeof(Action<object>),
        typeof(Action<Exception>),
        typeof(Action<IAsyncResult>),
        typeof(EventHandler),
        typeof(Func<object>),
        typeof(UnityAction),
        typeof(UnityAction<object>),
    };

    [BlackList] public static List<List<string>> BlackList = new List<List<string>>()
    {
        new List<string>() {"System.Type", "IsSZArray"}
    };

    [LuaCallCSharp] 
    [ReflectionUse] public static List<Type> dotween_lua_call_cs_list = new List<Type>()
    {
        typeof(DG.Tweening.AutoPlay),
        typeof(DG.Tweening.AxisConstraint),
        typeof(DG.Tweening.Ease),
        typeof(DG.Tweening.LogBehaviour),
        typeof(DG.Tweening.LoopType),
        typeof(DG.Tweening.PathMode),
        typeof(DG.Tweening.PathType),
        typeof(DG.Tweening.RotateMode),
        typeof(DG.Tweening.ScrambleMode),
        typeof(DG.Tweening.TweenType),
        typeof(DG.Tweening.UpdateType),

        typeof(DG.Tweening.DOTween),
        typeof(DG.Tweening.DOVirtual),
        typeof(DG.Tweening.EaseFactory),
        typeof(DG.Tweening.Tweener),
        typeof(DG.Tweening.Tween),
        typeof(DG.Tweening.Sequence),
        typeof(DG.Tweening.TweenParams),
        typeof(DG.Tweening.Core.ABSSequentiable),

        typeof(DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions>),

        typeof(DG.Tweening.TweenCallback),
        typeof(DG.Tweening.TweenExtensions),
        typeof(DG.Tweening.TweenSettingsExtensions),
        typeof(DG.Tweening.ShortcutExtensions),
        typeof(TMPro.TMP_InputField),
        typeof(TMPro.TextMeshProUGUI),

        //dotween pro 的功能
        //typeof(DG.Tweening.DOTweenPath),
        //typeof(DG.Tweening.DOTweenVisualManager),
    };
    
}