require("System")
local M = class("QuitPopup", target)
---@type Button
local closeBtn
---@type Application
local Application = CS.UnityEngine.Application;
function M:start()
    ---@type Button
    closeBtn = self.closeGo:GetComponent("Button");
    closeBtn.onClick:AddListener(function()
        Application.Quit()
    end)
end
return M