using UnityEngine;

public class UILogin : UIView
{
    public void ShowLobby()
    {
        UIManager.Instance.OpenScreen("Lobby");
    }
}