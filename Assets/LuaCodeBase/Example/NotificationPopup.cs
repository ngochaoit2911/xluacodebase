using UnityEngine;

public class NotificationPopup : UIPopup
{
    public void Accept()
    {
        this.Close();
        UIManager.Instance.OpenScreen("Login");
    }

    public void Deny()
    {
        this.Close();
    }
}