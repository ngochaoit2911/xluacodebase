using System;
using Sirenix.OdinInspector;
using UnityEngine;
using XLua;

public class BasicRunLua : MonoBehaviour
{
    private LuaEnv env;
    private LuaTable luaTable;
    public TextAsset luaScript;

    [CSharpCallLua]
    public delegate int IntAdd(int x, int y);

    public GameObject btnGo;

    private Action onStart;

    private void Awake()
    {
        RunLua();
    }

    private void Start()
    {
        onStart?.Invoke();
    }

    [Button]
    public void RunLua()
    {
        env = new LuaEnv();
        luaTable = env.NewTable();
        LuaTable meta = env.NewTable();
        meta.Set("__index", env.Global);
        luaTable.SetMetaTable(meta);
        meta.Dispose();
        luaTable.Set("self", this);

        // Gán giá trị btnGo vào index "btnGo" luaTable => luaTable["btnGo"] = btnGo hoặc luaTable.btnGo = btnGo
        luaTable.Set("btnGo", btnGo);
        env.DoString(luaScript.text, "chunk", luaTable);
        // var addAction = luaTable.Get<IntAdd>("Add");
        // var value = addAction.Invoke(10, 20);
        // Debug.Log("Value: " + value);
        onStart = luaTable.Get<Action>("start");
        
        // var coroutine = luaTable.Get<Action<int>>("TestCoroutine");
        // coroutine?.Invoke(10);
        //
        // var dotween = luaTable.Get<Action>("TestWithDoTween");
        // dotween?.Invoke();
    }
}