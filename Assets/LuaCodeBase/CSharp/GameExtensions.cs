using System;
using System.Collections;
using libx;
using UnityEngine;
using XLua;

[LuaCallCSharp()]
public static class GameExtensions
{
    public static T Cast<T>(this MonoBehaviour mono) where T : class
    {
        var t = mono as T;
        return t;
    }

    private static IEnumerator LoadAsset(string path, Type type)
    {
        var request = LoadAsRequest(path, type);
        yield return request;
        if (!string.IsNullOrEmpty(request.error))
        {
            request.Release();
            yield break;
        }
    }

    static AssetRequest LoadAsRequest (string path, Type type)
    {
        var request = Assets.LoadAsset (path, type);
        return request;
    }
}