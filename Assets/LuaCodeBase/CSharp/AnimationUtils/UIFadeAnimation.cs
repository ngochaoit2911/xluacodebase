using System;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UIFadeAnimation : UIDefaultAnimation
{
    public float fadeTime = 0.25f;
    [SerializeField]
    protected CanvasGroup canvasGroup;

    private void OnValidate()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public override Sequence OnStart()
    {
        mainSequence =  base.OnStart();
        canvasGroup.alpha = 0;
        mainSequence.Append(canvasGroup.DOFade(1, fadeTime));
        mainSequence.Restart();
        return mainSequence;
    }

    public override Sequence OnReverse()
    {
        mainSequence = base.OnReverse();
        canvasGroup.alpha = 1;
        mainSequence.Append(canvasGroup.DOFade(0, fadeTime));
        mainSequence.Restart();
        return mainSequence;
    }
}