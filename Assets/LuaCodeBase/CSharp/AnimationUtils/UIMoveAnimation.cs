using DG.Tweening;
using UnityEngine;

public class UIMoveAnimation : UIBaseAnimation
{
    public MoveType moveType = MoveType.MoveY;
    public float startPos = 300, middlePos = -10, endPos = 0;
    public float firstTime = 0.2f, secondTime = 0.02f;

    public override Sequence OnStart()
    {
        mainSequence = base.OnStart();
        canvasGroup.alpha = 0;
        if (moveType == MoveType.MoveX)
        {
            mainSequence.Append(content.DOAnchorPosX(middlePos, firstTime))
                .Join(canvasGroup.DOFade(1, firstTime))
                .Append(content.DOAnchorPosX(endPos, secondTime));
            mainSequence.Restart();
        }
        else
        {
            mainSequence.Append(content.DOAnchorPosY(middlePos, firstTime))
                .Join(canvasGroup.DOFade(1, firstTime))
                .Append(content.DOAnchorPosY(endPos, secondTime));
            mainSequence.Restart();
        }
        return mainSequence;
    }

    public override Sequence OnReverse()
    {
        mainSequence = base.OnReverse();
        canvasGroup.alpha = 1;
        if (moveType == MoveType.MoveX)
        {
            mainSequence.Append(content.DOAnchorPosX(middlePos, secondTime))
                .Append(canvasGroup.DOFade(0, firstTime))
                .Join(content.DOAnchorPosX(startPos, firstTime));
            mainSequence.Restart();
        }
        else
        {
            mainSequence.Append(content.DOAnchorPosY(middlePos, secondTime))
                .Append(canvasGroup.DOFade(0, firstTime))
                .Join(content.DOAnchorPosY(startPos, firstTime));
            mainSequence.Restart();
        }

        return mainSequence;
    }
}

public enum MoveType
{
    MoveX,
    MoveY
}