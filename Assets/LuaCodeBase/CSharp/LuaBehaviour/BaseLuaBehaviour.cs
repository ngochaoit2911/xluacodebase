using System;
using LuaFramework;
using UnityEngine;
using XLua;

public class BaseLuaBehaviour : MonoBehaviour, IInitLuaScript, ILuaExtendable
{
    public ScriptReference script;
        public VariableArray variables;

        protected LuaTable scriptEnv;
        protected LuaTable metatable;
        protected Action<MonoBehaviour> onAwake;
        protected Action<MonoBehaviour> onEnable;
        protected Action<MonoBehaviour> onDisable;
        protected Func<MonoBehaviour, ILuaTask> onStart;
        protected Action<MonoBehaviour> onUpdate;
        protected Action<MonoBehaviour> onDestroy;

        public virtual LuaTable GetMetatable()
        {
            return this.metatable;
        }

        public virtual void Initialize()
        {
            var luaEnv = LuaEnvironment.LuaEnv;
            scriptEnv = luaEnv.NewTable();

            LuaTable meta = luaEnv.NewTable();
            meta.Set("__index", luaEnv.Global);
            scriptEnv.SetMetaTable(meta);
            meta.Dispose();

            scriptEnv.Set("target", this);

            string scriptText = (script.Type == ScriptReferenceType.TextAsset) 
                ? script.Text.text 
                : string.Format("require(\"System\");local cls = require(\"{0}\");return extends(target,cls);", script.Filename);
            object[] result = luaEnv.DoString(scriptText, string.Format("{0}({1})", "LuaBehaviour", this.name), scriptEnv);

            if (result.Length != 1 || !(result[0] is LuaTable))
                throw new Exception("");

            metatable = (LuaTable)result[0];
            if (variables != null && variables.Variables != null)
            {
                foreach (var variable in variables.Variables)
                {
                    var name = variable.Name.Trim();
                    if (string.IsNullOrEmpty(name))
                        continue;
                    Debug.Log(variable.GetValue());
                    metatable.Set(name, variable.GetValue());
                }
            }

            onAwake = metatable.Get<Action<MonoBehaviour>>("awake");
            onEnable = metatable.Get<Action<MonoBehaviour>>("enable");
            onDisable = metatable.Get<Action<MonoBehaviour>>("disable");
            onStart = metatable.Get<Func<MonoBehaviour, ILuaTask>>("start");
            onUpdate = metatable.Get<Action<MonoBehaviour>>("update");
            onDestroy = metatable.Get<Action<MonoBehaviour>>("destroy");
        }
        
        protected virtual void Awake()
        {
            this.Initialize();

            if (onAwake != null)
                onAwake(this);
        }

}