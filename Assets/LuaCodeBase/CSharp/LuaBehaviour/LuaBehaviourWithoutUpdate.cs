using LuaFramework;
using UnityEngine;

public class LuaBehaviourWithoutUpdate : BaseLuaBehaviour
{
    private void OnEnable()
    {
        if (onEnable != null)
            onEnable(this);
    }

    private void OnDisable()
    {
        if (onDisable != null)
            onDisable(this);
    }

    private async void Start()
    {
        if (onStart != null)
        {
            ILuaTask task = onStart(this);
            if (task != null)
                await task;
        }
    }

    private void OnDestroy()
    {
        if (onDestroy != null)
            onDestroy(this);

        onDestroy = null;
        onUpdate = null;
        onStart = null;
        onEnable = null;
        onDisable = null;
        onAwake = null;

        if (metatable != null)
        {
            metatable.Dispose();
            metatable = null;
        }

        if (scriptEnv != null)
        {
            scriptEnv.Dispose();
            scriptEnv = null;
        }
    }
}