
using XLua;

[LuaCallCSharp()]
public interface IAnimate
{
    void OnStart();
    void OnStop();
    void OnClose();
}