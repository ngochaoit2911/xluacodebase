using UnityEngine;

public abstract class UIBaseView : MonoBehaviour, IView
{
    public string ViewId { get; set; }

    public virtual void Open()
    {
        ViewCreator.Views.Add(this);
    }

    public virtual void Close()
    {
        ViewCreator.Views.Remove(this);
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }
}
