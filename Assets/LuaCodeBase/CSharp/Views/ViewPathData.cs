// Assembly-CSharp

using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


[CreateAssetMenu(fileName = "ViewData", menuName = "Data/Views", order = 0)]
public class ViewPathData : ScriptableObject
{
    public List<ViewPathInfo> views;

    public string GetPathById(string id)
    {
        return views.Find(s => s.viewId.Equals(id, StringComparison.OrdinalIgnoreCase)).path;
    }    
}

[System.Serializable]
public struct ViewPathInfo
{
    public string viewId;
    public string path;
}