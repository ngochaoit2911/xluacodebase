using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lean.Pool;
using libx;
using LuaFramework;

using UnityEngine;
using XLua;
using Object = System.Object;

[LuaCallCSharp()]
[ReflectionUse]
public class ViewCreator
{
    public static string ResourcePath = "Assets/LuaCodeBase/Data/ViewPathData.asset";
    public static ViewPathData PathData;
    public static List<UIBaseView> Views = new List<UIBaseView>();
    public static List<UIBaseView> Popups = new List<UIBaseView>();
    public static Dictionary<string, UIBaseView> Prefabs = new Dictionary<string, UIBaseView>();

    private static Coroutine LoadCoroutine;
    private static Transform _PopupParent;

    public static Transform PopupParent
    {
        get
        {
            if (!_PopupParent)
            {
                _PopupParent = GameObject.FindGameObjectWithTag("PopupHolder").transform;
            }

            return _PopupParent;
        }
    }

    public static void GetPopupByName(string name, Action<UIBaseView> onComplete = default, Transform parent = null)
    {
        var ct = IGetPopupByName(name, onComplete, parent);
        if (LoadCoroutine != null)
        {
            Executors.StopCoroutine(LoadCoroutine);
        }

        LoadCoroutine = Executors.RunOnCoroutineReturn(ct);
    }
    
    private static IEnumerator IGetPopupByName(string name, Action<UIBaseView> onComplete = default, Transform parent = null)
    {
        if (!PathData)
        {
            yield return GetViewPathData();
        }
        var path = PathData.GetPathById(name);
        if (!Prefabs.ContainsKey(path))
        {
            foreach (var p in Popups)
            {
                if (p.GetGameObject().activeInHierarchy)
                {
                    p.Close();
                } 
            }
            yield return GetViewPrefab(path, (o) =>
            {
                var v = o.GetComponent<UIPopup>();
                Prefabs.Add(path, v);
                var m = LeanPool.Spawn(Prefabs[path], !parent ? PopupParent : parent);
                m.ViewId = path;
                Popups.Add(m);
                m.Open();
                onComplete?.Invoke(m);
            });
        }
        else
        {
            UIBaseView cache = null;
            foreach (var p in Popups)
            {
                if (p.ViewId == path)
                {
                    cache = p;
                }
                else
                {
                    if (p.GetGameObject().activeInHierarchy)
                    {
                        p.Close();
                    }
                }
            }

            if (!cache)
            {
                Debug.Log("Null");
                yield break;
            }
            if (cache.GetGameObject().activeInHierarchy) yield break;
            var m = LeanPool.Spawn(Prefabs[path], !parent ? PopupParent : parent);
            m.Open();
            onComplete?.Invoke(m);
        }
    }
    
    #region Get Prefab

    private static IEnumerator GetViewPrefab(string path, Action<GameObject> onLoadComplete)
    {
        GameObject go = null;

        var request = libx.Assets.LoadAsset(path, typeof(GameObject));
        yield return request;
        if (!string.IsNullOrEmpty(request.error))
        {
            request.Release();
            yield break;
        }

        go = request.asset as GameObject;
        onLoadComplete.Invoke(go);

    }

    public static IEnumerator GetViewPathData()
    {
        var request = libx.Assets.LoadAsset(ResourcePath, typeof(ViewPathData));
        request.completed += assetRequest => { PathData = assetRequest.asset as ViewPathData; };
        yield return request;
    }

    #endregion

}