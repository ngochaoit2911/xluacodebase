using System;
using System.Collections.Generic;
using DG.Tweening;
using LuaFramework;
using UnityEngine;
using Sirenix.OdinInspector;
using XLua;

// public enum ToTomScreenId
// {
//     UNKNOW = 0,
//     LOGIN = 1,
//     LOBBY = 2,
//     GAME_PLAY = 3,
//     SELECT_ROOM = 4,
//     HOC_CHOI_SCREEN = 5,
//     HOC_CHOI_GAMEPLAY = 6,
// }

[Serializable]
public class ToTomScreen
{
    //public bool isLoaded;
    public UIView screen = null;
    public string screenId;
    //public List<ButtonGroupType> hideButtonTypes;
}

[LuaCallCSharp()]
public class UIManager : MonoBehaviour
{
    [SerializeField] private List<ToTomScreen> listScreen = new List<ToTomScreen>();
    private ToTomScreen currentScreen;

    public static UIManager Instance
    {
        get
        {
            if (_instance) return _instance;
            _instance = FindObjectOfType<UIManager>();
            return _instance;
        }
    }

    private static UIManager _instance;
    public ViewPathData pathData;

    private Sequence flashSequence;
    private Action screenAction = () => { };

    private void Awake()
    {
#if !UNITY_WEBGL
        Application.targetFrameRate = 60;
#endif
    }

    private void Start()
    {
        //this.ShowButtonGroup(listScreen[0].hideButtonTypes);
        OpenScreen("Login");
    }

    public UIView OpenScreen(string screenId, Transform parent = null, bool isShowFlash = true)
    {
        if (currentScreen != null && screenId == currentScreen.screenId)
        {
            return currentScreen.screen;
        }

        currentScreen = null;
        screenAction = null;
        UIView p = null;
        foreach (var obj in listScreen)
        {
            if (obj.screenId == screenId)
            {
                
                    p = obj.screen;
                    currentScreen = obj;
                
            }

            screenAction += () =>
            {
                obj.screen.gameObject.SetActive(obj.screenId == screenId);
                if (obj.screenId == screenId)
                {
                    //this.ShowButtonGroup(obj.hideButtonTypes);
                }
            };
        }

        if (isShowFlash)
        {
            ShowFlash();
        }
        else
        {
            screenAction?.Invoke();
        }
        //LogEvent(screenId);
        return p;
    }

    [Button]
    private void ShowFlash()
    {
        screenAction.Invoke();
        //this.ShowFlashWithCallBack(screenAction.Invoke);
    }

    public void OpenPopup(string name, Action<UIBaseView> onLoadComplete, Transform parent = null)
    {
        UIPopup cache = null;
        foreach (var popup in ViewCreator.Popups)
        {
            if (popup.ViewId != name)
            {
                if (popup.gameObject.activeInHierarchy)
                {
                    popup.Close();
                }
            }
            else
            {
                if (popup.gameObject.activeInHierarchy)
                {
                    Debug.Log(popup.gameObject.activeInHierarchy + "   " + popup.name);
                    cache = popup.Cast<UIPopup>();
                }
            }
        }

        if (cache)
        {
            onLoadComplete.Invoke(cache);
        }
        else
        {
            ViewCreator.GetPopupByName(name, onLoadComplete, parent);
        }
    }

}