
using System;
using UnityEngine;
using XLua;

[LuaCallCSharp()]
[ReflectionUse]
public interface IView
{
    string ViewId { get; set; }
    void Open();
    void Close();
    GameObject GetGameObject();
}