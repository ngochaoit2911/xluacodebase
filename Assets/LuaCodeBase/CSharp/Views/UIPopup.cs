using DG.Tweening;
using Lean.Pool;
using UnityEngine;
using XLua;

[LuaCallCSharp()]
public class UIPopup : UIBaseView, IAnimate
{
    [SerializeField] protected UIDefaultAnimation popupAnim;

    private void OnValidate()
    {
        popupAnim = GetComponent<UIDefaultAnimation>();
        if (!popupAnim)
        {
            popupAnim = gameObject.AddComponent<UIScaleAnimation>();
        }
    }
    
    public override void Open()
    {
        base.Open();
        OnStart();
    }

    public override void Close()
    {
        base.Open();
        OnClose();
    }
    
    public void OnStart()
    {
        popupAnim.OnStop();
        popupAnim.OnStart();
    }

    public void OnStop()
    {
        
    }
    public void OnClose()
    {
        popupAnim.OnStop();
        popupAnim.OnReverse().OnComplete(() =>
        {
            LeanPool.Despawn(this);
        });
    }
    
}