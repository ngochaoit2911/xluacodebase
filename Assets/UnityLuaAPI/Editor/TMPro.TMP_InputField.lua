﻿---@class TMP_InputField : Selectable
---@field public shouldHideMobileInput boolean
---@field public shouldHideSoftKeyboard boolean
---@field public text string
---@field public isFocused boolean
---@field public caretBlinkRate Single
---@field public caretWidth Int32
---@field public textViewport RectTransform
---@field public textComponent TMP_Text
---@field public placeholder Graphic
---@field public verticalScrollbar Scrollbar
---@field public scrollSensitivity Single
---@field public caretColor Color
---@field public customCaretColor boolean
---@field public selectionColor Color
---@field public onEndEdit SubmitEvent
---@field public onSubmit SubmitEvent
---@field public onSelect SelectionEvent
---@field public onDeselect SelectionEvent
---@field public onTextSelection TextSelectionEvent
---@field public onEndTextSelection TextSelectionEvent
---@field public onValueChanged OnChangeEvent
---@field public onTouchScreenKeyboardStatusChanged TouchScreenKeyboardEvent
---@field public onValidateInput OnValidateInput
---@field public characterLimit Int32
---@field public pointSize Single
---@field public fontAsset TMP_FontAsset
---@field public onFocusSelectAll boolean
---@field public resetOnDeActivation boolean
---@field public restoreOriginalTextOnEscape boolean
---@field public isRichTextEditingAllowed boolean
---@field public contentType number
---@field public lineType number
---@field public lineLimit Int32
---@field public inputType number
---@field public keyboardType number
---@field public characterValidation number
---@field public inputValidator TMP_InputValidator
---@field public readOnly boolean
---@field public richText boolean
---@field public multiLine boolean
---@field public asteriskChar Char
---@field public wasCanceled boolean
---@field public caretPosition Int32
---@field public selectionAnchorPosition Int32
---@field public selectionFocusPosition Int32
---@field public stringPosition Int32
---@field public selectionStringAnchorPosition Int32
---@field public selectionStringFocusPosition Int32
---@field public minWidth Single
---@field public preferredWidth Single
---@field public flexibleWidth Single
---@field public minHeight Single
---@field public preferredHeight Single
---@field public flexibleHeight Single
---@field public layoutPriority Int32
local TMP_InputField={ }
---@public
---@param input string
---@return void
function TMP_InputField:SetTextWithoutNotify(input) end
---@public
---@param shift boolean
---@return void
function TMP_InputField:MoveTextEnd(shift) end
---@public
---@param shift boolean
---@return void
function TMP_InputField:MoveTextStart(shift) end
---@public
---@param shift boolean
---@param ctrl boolean
---@return void
function TMP_InputField:MoveToEndOfLine(shift, ctrl) end
---@public
---@param shift boolean
---@param ctrl boolean
---@return void
function TMP_InputField:MoveToStartOfLine(shift, ctrl) end
---@public
---@param eventData PointerEventData
---@return void
function TMP_InputField:OnBeginDrag(eventData) end
---@public
---@param eventData PointerEventData
---@return void
function TMP_InputField:OnDrag(eventData) end
---@public
---@param eventData PointerEventData
---@return void
function TMP_InputField:OnEndDrag(eventData) end
---@public
---@param eventData PointerEventData
---@return void
function TMP_InputField:OnPointerDown(eventData) end
---@public
---@param e Event
---@return void
function TMP_InputField:ProcessEvent(e) end
---@public
---@param eventData BaseEventData
---@return void
function TMP_InputField:OnUpdateSelected(eventData) end
---@public
---@param eventData PointerEventData
---@return void
function TMP_InputField:OnScroll(eventData) end
---@public
---@return void
function TMP_InputField:ForceLabelUpdate() end
---@public
---@param update number
---@return void
function TMP_InputField:Rebuild(update) end
---@public
---@return void
function TMP_InputField:LayoutComplete() end
---@public
---@return void
function TMP_InputField:GraphicUpdateComplete() end
---@public
---@return void
function TMP_InputField:ActivateInputField() end
---@public
---@param eventData BaseEventData
---@return void
function TMP_InputField:OnSelect(eventData) end
---@public
---@param eventData PointerEventData
---@return void
function TMP_InputField:OnPointerClick(eventData) end
---@public
---@return void
function TMP_InputField:OnControlClick() end
---@public
---@return void
function TMP_InputField:ReleaseSelection() end
---@public
---@param clearSelection boolean
---@return void
function TMP_InputField:DeactivateInputField(clearSelection) end
---@public
---@param eventData BaseEventData
---@return void
function TMP_InputField:OnDeselect(eventData) end
---@public
---@param eventData BaseEventData
---@return void
function TMP_InputField:OnSubmit(eventData) end
---@public
---@return void
function TMP_InputField:CalculateLayoutInputHorizontal() end
---@public
---@return void
function TMP_InputField:CalculateLayoutInputVertical() end
---@public
---@param pointSize Single
---@return void
function TMP_InputField:SetGlobalPointSize(pointSize) end
---@public
---@param fontAsset TMP_FontAsset
---@return void
function TMP_InputField:SetGlobalFontAsset(fontAsset) end
TMPro.TMP_InputField = TMP_InputField