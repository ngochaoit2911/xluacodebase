﻿---@class UnityEvent`1 : UnityEventBase
local UnityEvent`1={ }
---@public
---@param call UnityAction`1
---@return void
function UnityEvent`1:AddListener(call) end
---@public
---@param call UnityAction`1
---@return void
function UnityEvent`1:RemoveListener(call) end
---@public
---@param arg0 Object
---@return void
function UnityEvent`1:Invoke(arg0) end
UnityEngine.Events.UnityEvent`1 = UnityEvent`1