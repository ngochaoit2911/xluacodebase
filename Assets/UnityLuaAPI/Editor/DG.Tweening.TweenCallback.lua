﻿---@class TweenCallback : MulticastDelegate
local TweenCallback={ }
---@public
---@return void
function TweenCallback:Invoke() end
---@public
---@param callback AsyncCallback
---@param object Object
---@return IAsyncResult
function TweenCallback:BeginInvoke(callback, object) end
---@public
---@param result IAsyncResult
---@return void
function TweenCallback:EndInvoke(result) end
DG.Tweening.TweenCallback = TweenCallback