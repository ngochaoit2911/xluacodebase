﻿---@class IView
---@field public ViewId string
local IView={ }
---@public
---@return void
function IView:Open() end
---@public
---@return void
function IView:Close() end
---@public
---@return GameObject
function IView:GetGameObject() end
.IView = IView