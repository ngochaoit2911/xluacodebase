﻿---@class IAnimate
local IAnimate={ }
---@public
---@return void
function IAnimate:OnStart() end
---@public
---@return void
function IAnimate:OnStop() end
---@public
---@return void
function IAnimate:OnClose() end
.IAnimate = IAnimate