﻿---@class Keyframe : ValueType
---@field public time Single
---@field public value Single
---@field public inTangent Single
---@field public outTangent Single
---@field public inWeight Single
---@field public outWeight Single
---@field public weightedMode number
---@field public tangentMode Int32
local Keyframe={ }
UnityEngine.Keyframe = Keyframe