﻿---@class AssetRequest : Reference
---@field public assetType Type
---@field public completed Action`1
---@field public name string
---@field public loadState number
---@field public isDone boolean
---@field public progress Single
---@field public error string
---@field public text string
---@field public bytes Byte[]
---@field public asset Object
---@field public Current Object
local AssetRequest={ }
---@public
---@return boolean
function AssetRequest:MoveNext() end
---@public
---@return void
function AssetRequest:Reset() end
libx.AssetRequest = AssetRequest