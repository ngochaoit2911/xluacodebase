﻿---@class UIPopup
local UIPopup={ }
---@public
---@return void
function UIPopup:Open() end
---@public
---@return void
function UIPopup:Close() end
---@public
---@return void
function UIPopup:OnStart() end
---@public
---@return void
function UIPopup:OnStop() end
---@public
---@return void
function UIPopup:OnClose() end
.UIPopup = UIPopup