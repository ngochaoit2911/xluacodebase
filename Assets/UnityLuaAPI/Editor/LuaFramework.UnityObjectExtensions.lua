﻿---@class UnityObjectExtensions
local UnityObjectExtensions={ }
---@public
---@param o Object
---@return boolean
function UnityObjectExtensions.IsDestroyed(o) end
LuaFramework.UnityObjectExtensions = UnityObjectExtensions