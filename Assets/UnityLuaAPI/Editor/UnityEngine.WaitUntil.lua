﻿---@class WaitUntil : CustomYieldInstruction
---@field public keepWaiting boolean
local WaitUntil={ }
UnityEngine.WaitUntil = WaitUntil