﻿---@class WaitForSecondsRealtime : CustomYieldInstruction
---@field public waitTime Single
---@field public keepWaiting boolean
local WaitForSecondsRealtime={ }
UnityEngine.WaitForSecondsRealtime = WaitForSecondsRealtime