﻿---@class Int32 : ValueType
---@field public MaxValue Int32
---@field public MinValue Int32
local Int32={ }
---@public
---@param value Object
---@return Int32
function Int32:CompareTo(value) end
---@public
---@param value Int32
---@return Int32
function Int32:CompareTo(value) end
---@public
---@param obj Object
---@return boolean
function Int32:Equals(obj) end
---@public
---@param obj Int32
---@return boolean
function Int32:Equals(obj) end
---@public
---@return Int32
function Int32:GetHashCode() end
---@public
---@return string
function Int32:ToString() end
---@public
---@param format string
---@return string
function Int32:ToString(format) end
---@public
---@param provider IFormatProvider
---@return string
function Int32:ToString(provider) end
---@public
---@param format string
---@param provider IFormatProvider
---@return string
function Int32:ToString(format, provider) end
---@public
---@param s string
---@return Int32
function Int32.Parse(s) end
---@public
---@param s string
---@param style number
---@return Int32
function Int32.Parse(s, style) end
---@public
---@param s string
---@param provider IFormatProvider
---@return Int32
function Int32.Parse(s, provider) end
---@public
---@param s string
---@param style number
---@param provider IFormatProvider
---@return Int32
function Int32.Parse(s, style, provider) end
---@public
---@param s string
---@param result Int32&
---@return boolean
function Int32.TryParse(s, result) end
---@public
---@param s string
---@param style number
---@param provider IFormatProvider
---@param result Int32&
---@return boolean
function Int32.TryParse(s, style, provider, result) end
---@public
---@return number
function Int32:GetTypeCode() end
System.Int32 = Int32