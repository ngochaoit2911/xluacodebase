﻿---@class Manifest : ScriptableObject
---@field public activeVariants String[]
---@field public dirs String[]
---@field public assets AssetRef[]
---@field public bundles BundleRef[]
local Manifest={ }
libx.Manifest = Manifest