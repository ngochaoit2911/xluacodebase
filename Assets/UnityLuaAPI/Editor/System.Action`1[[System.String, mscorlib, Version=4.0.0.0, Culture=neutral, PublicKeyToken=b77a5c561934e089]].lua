﻿---@class Action`1 : MulticastDelegate
local Action`1={ }
---@public
---@param obj string
---@return void
function Action`1:Invoke(obj) end
---@public
---@param obj string
---@param callback AsyncCallback
---@param object Object
---@return IAsyncResult
function Action`1:BeginInvoke(obj, callback, object) end
---@public
---@param result IAsyncResult
---@return void
function Action`1:EndInvoke(result) end
System.Action`1 = Action`1