﻿---@class BaseEventData : AbstractEventData
---@field public currentInputModule BaseInputModule
---@field public selectedObject GameObject
local BaseEventData={ }
UnityEngine.EventSystems.BaseEventData = BaseEventData