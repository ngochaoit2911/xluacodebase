﻿---@class LuaBehaviour
---@field public script ScriptReference
---@field public variables VariableArray
local LuaBehaviour={ }
---@public
---@return LuaTable
function LuaBehaviour:GetMetatable() end
LuaFramework.LuaBehaviour = LuaBehaviour