﻿---@class UIManager
---@field public pathData ViewPathData
---@field public Instance UIManager
local UIManager={ }
---@public
---@param screenId string
---@param parent Transform
---@param isShowFlash boolean
---@return UIView
function UIManager:OpenScreen(screenId, parent, isShowFlash) end
---@public
---@param name string
---@param onLoadComplete Action`1
---@param parent Transform
---@return void
function UIManager:OpenPopup(name, onLoadComplete, parent) end
.UIManager = UIManager