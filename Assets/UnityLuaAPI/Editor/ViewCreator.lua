﻿---@class ViewCreator
---@field public ResourcePath string
---@field public PathData ViewPathData
---@field public Views List`1
---@field public Popups List`1
---@field public Prefabs Dictionary`2
---@field public PopupParent Transform
local ViewCreator={ }
---@public
---@param name string
---@param onComplete Action`1
---@param parent Transform
---@return void
function ViewCreator.GetPopupByName(name, onComplete, parent) end
---@public
---@return IEnumerator
function ViewCreator.GetViewPathData() end
.ViewCreator = ViewCreator