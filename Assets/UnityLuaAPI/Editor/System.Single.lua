﻿---@class Single : ValueType
---@field public MinValue Single
---@field public Epsilon Single
---@field public MaxValue Single
---@field public PositiveInfinity Single
---@field public NegativeInfinity Single
---@field public NaN Single
local Single={ }
---@public
---@param f Single
---@return boolean
function Single.IsInfinity(f) end
---@public
---@param f Single
---@return boolean
function Single.IsPositiveInfinity(f) end
---@public
---@param f Single
---@return boolean
function Single.IsNegativeInfinity(f) end
---@public
---@param f Single
---@return boolean
function Single.IsNaN(f) end
---@public
---@param f Single
---@return boolean
function Single.IsFinite(f) end
---@public
---@param value Object
---@return Int32
function Single:CompareTo(value) end
---@public
---@param value Single
---@return Int32
function Single:CompareTo(value) end
---@public
---@param left Single
---@param right Single
---@return boolean
function Single.op_Equality(left, right) end
---@public
---@param left Single
---@param right Single
---@return boolean
function Single.op_Inequality(left, right) end
---@public
---@param left Single
---@param right Single
---@return boolean
function Single.op_LessThan(left, right) end
---@public
---@param left Single
---@param right Single
---@return boolean
function Single.op_GreaterThan(left, right) end
---@public
---@param left Single
---@param right Single
---@return boolean
function Single.op_LessThanOrEqual(left, right) end
---@public
---@param left Single
---@param right Single
---@return boolean
function Single.op_GreaterThanOrEqual(left, right) end
---@public
---@param obj Object
---@return boolean
function Single:Equals(obj) end
---@public
---@param obj Single
---@return boolean
function Single:Equals(obj) end
---@public
---@return Int32
function Single:GetHashCode() end
---@public
---@return string
function Single:ToString() end
---@public
---@param provider IFormatProvider
---@return string
function Single:ToString(provider) end
---@public
---@param format string
---@return string
function Single:ToString(format) end
---@public
---@param format string
---@param provider IFormatProvider
---@return string
function Single:ToString(format, provider) end
---@public
---@param s string
---@return Single
function Single.Parse(s) end
---@public
---@param s string
---@param style number
---@return Single
function Single.Parse(s, style) end
---@public
---@param s string
---@param provider IFormatProvider
---@return Single
function Single.Parse(s, provider) end
---@public
---@param s string
---@param style number
---@param provider IFormatProvider
---@return Single
function Single.Parse(s, style, provider) end
---@public
---@param s string
---@param result Single&
---@return boolean
function Single.TryParse(s, result) end
---@public
---@param s string
---@param style number
---@param provider IFormatProvider
---@param result Single&
---@return boolean
function Single.TryParse(s, style, provider, result) end
---@public
---@return number
function Single:GetTypeCode() end
System.Single = Single