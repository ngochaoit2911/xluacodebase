﻿---@class Boolean : ValueType
---@field public TrueString string
---@field public FalseString string
local Boolean={ }
---@public
---@return Int32
function Boolean:GetHashCode() end
---@public
---@return string
function Boolean:ToString() end
---@public
---@param provider IFormatProvider
---@return string
function Boolean:ToString(provider) end
---@public
---@param obj Object
---@return boolean
function Boolean:Equals(obj) end
---@public
---@param obj boolean
---@return boolean
function Boolean:Equals(obj) end
---@public
---@param obj Object
---@return Int32
function Boolean:CompareTo(obj) end
---@public
---@param value boolean
---@return Int32
function Boolean:CompareTo(value) end
---@public
---@param value string
---@return boolean
function Boolean.Parse(value) end
---@public
---@param value string
---@param result Boolean&
---@return boolean
function Boolean.TryParse(value, result) end
---@public
---@return number
function Boolean:GetTypeCode() end
System.Boolean = Boolean