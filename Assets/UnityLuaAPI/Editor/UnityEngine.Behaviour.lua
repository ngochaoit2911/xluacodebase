﻿---@class Behaviour : Component
---@field public enabled boolean
---@field public isActiveAndEnabled boolean
local Behaviour={ }
UnityEngine.Behaviour = Behaviour