﻿---@class Mathf : ValueType
---@field public PI Single
---@field public Infinity Single
---@field public NegativeInfinity Single
---@field public Deg2Rad Single
---@field public Rad2Deg Single
---@field public Epsilon Single
local Mathf={ }
---@public
---@param value Int32
---@return Int32
function Mathf.ClosestPowerOfTwo(value) end
---@public
---@param value Int32
---@return boolean
function Mathf.IsPowerOfTwo(value) end
---@public
---@param value Int32
---@return Int32
function Mathf.NextPowerOfTwo(value) end
---@public
---@param value Single
---@return Single
function Mathf.GammaToLinearSpace(value) end
---@public
---@param value Single
---@return Single
function Mathf.LinearToGammaSpace(value) end
---@public
---@param kelvin Single
---@return Color
function Mathf.CorrelatedColorTemperatureToRGB(kelvin) end
---@public
---@param val Single
---@return UInt16
function Mathf.FloatToHalf(val) end
---@public
---@param val UInt16
---@return Single
function Mathf.HalfToFloat(val) end
---@public
---@param x Single
---@param y Single
---@return Single
function Mathf.PerlinNoise(x, y) end
---@public
---@param f Single
---@return Single
function Mathf.Sin(f) end
---@public
---@param f Single
---@return Single
function Mathf.Cos(f) end
---@public
---@param f Single
---@return Single
function Mathf.Tan(f) end
---@public
---@param f Single
---@return Single
function Mathf.Asin(f) end
---@public
---@param f Single
---@return Single
function Mathf.Acos(f) end
---@public
---@param f Single
---@return Single
function Mathf.Atan(f) end
---@public
---@param y Single
---@param x Single
---@return Single
function Mathf.Atan2(y, x) end
---@public
---@param f Single
---@return Single
function Mathf.Sqrt(f) end
---@public
---@param f Single
---@return Single
function Mathf.Abs(f) end
---@public
---@param value Int32
---@return Int32
function Mathf.Abs(value) end
---@public
---@param a Single
---@param b Single
---@return Single
function Mathf.Min(a, b) end
---@public
---@param values Single[]
---@return Single
function Mathf.Min(values) end
---@public
---@param a Int32
---@param b Int32
---@return Int32
function Mathf.Min(a, b) end
---@public
---@param values Int32[]
---@return Int32
function Mathf.Min(values) end
---@public
---@param a Single
---@param b Single
---@return Single
function Mathf.Max(a, b) end
---@public
---@param values Single[]
---@return Single
function Mathf.Max(values) end
---@public
---@param a Int32
---@param b Int32
---@return Int32
function Mathf.Max(a, b) end
---@public
---@param values Int32[]
---@return Int32
function Mathf.Max(values) end
---@public
---@param f Single
---@param p Single
---@return Single
function Mathf.Pow(f, p) end
---@public
---@param power Single
---@return Single
function Mathf.Exp(power) end
---@public
---@param f Single
---@param p Single
---@return Single
function Mathf.Log(f, p) end
---@public
---@param f Single
---@return Single
function Mathf.Log(f) end
---@public
---@param f Single
---@return Single
function Mathf.Log10(f) end
---@public
---@param f Single
---@return Single
function Mathf.Ceil(f) end
---@public
---@param f Single
---@return Single
function Mathf.Floor(f) end
---@public
---@param f Single
---@return Single
function Mathf.Round(f) end
---@public
---@param f Single
---@return Int32
function Mathf.CeilToInt(f) end
---@public
---@param f Single
---@return Int32
function Mathf.FloorToInt(f) end
---@public
---@param f Single
---@return Int32
function Mathf.RoundToInt(f) end
---@public
---@param f Single
---@return Single
function Mathf.Sign(f) end
---@public
---@param value Single
---@param min Single
---@param max Single
---@return Single
function Mathf.Clamp(value, min, max) end
---@public
---@param value Int32
---@param min Int32
---@param max Int32
---@return Int32
function Mathf.Clamp(value, min, max) end
---@public
---@param value Single
---@return Single
function Mathf.Clamp01(value) end
---@public
---@param a Single
---@param b Single
---@param t Single
---@return Single
function Mathf.Lerp(a, b, t) end
---@public
---@param a Single
---@param b Single
---@param t Single
---@return Single
function Mathf.LerpUnclamped(a, b, t) end
---@public
---@param a Single
---@param b Single
---@param t Single
---@return Single
function Mathf.LerpAngle(a, b, t) end
---@public
---@param current Single
---@param target Single
---@param maxDelta Single
---@return Single
function Mathf.MoveTowards(current, target, maxDelta) end
---@public
---@param current Single
---@param target Single
---@param maxDelta Single
---@return Single
function Mathf.MoveTowardsAngle(current, target, maxDelta) end
---@public
---@param from Single
---@param to Single
---@param t Single
---@return Single
function Mathf.SmoothStep(from, to, t) end
---@public
---@param value Single
---@param absmax Single
---@param gamma Single
---@return Single
function Mathf.Gamma(value, absmax, gamma) end
---@public
---@param a Single
---@param b Single
---@return boolean
function Mathf.Approximately(a, b) end
---@public
---@param current Single
---@param target Single
---@param currentVelocity Single&
---@param smoothTime Single
---@param maxSpeed Single
---@return Single
function Mathf.SmoothDamp(current, target, currentVelocity, smoothTime, maxSpeed) end
---@public
---@param current Single
---@param target Single
---@param currentVelocity Single&
---@param smoothTime Single
---@return Single
function Mathf.SmoothDamp(current, target, currentVelocity, smoothTime) end
---@public
---@param current Single
---@param target Single
---@param currentVelocity Single&
---@param smoothTime Single
---@param maxSpeed Single
---@param deltaTime Single
---@return Single
function Mathf.SmoothDamp(current, target, currentVelocity, smoothTime, maxSpeed, deltaTime) end
---@public
---@param current Single
---@param target Single
---@param currentVelocity Single&
---@param smoothTime Single
---@param maxSpeed Single
---@return Single
function Mathf.SmoothDampAngle(current, target, currentVelocity, smoothTime, maxSpeed) end
---@public
---@param current Single
---@param target Single
---@param currentVelocity Single&
---@param smoothTime Single
---@return Single
function Mathf.SmoothDampAngle(current, target, currentVelocity, smoothTime) end
---@public
---@param current Single
---@param target Single
---@param currentVelocity Single&
---@param smoothTime Single
---@param maxSpeed Single
---@param deltaTime Single
---@return Single
function Mathf.SmoothDampAngle(current, target, currentVelocity, smoothTime, maxSpeed, deltaTime) end
---@public
---@param t Single
---@param length Single
---@return Single
function Mathf.Repeat(t, length) end
---@public
---@param t Single
---@param length Single
---@return Single
function Mathf.PingPong(t, length) end
---@public
---@param a Single
---@param b Single
---@param value Single
---@return Single
function Mathf.InverseLerp(a, b, value) end
---@public
---@param current Single
---@param target Single
---@return Single
function Mathf.DeltaAngle(current, target) end
UnityEngine.Mathf = Mathf