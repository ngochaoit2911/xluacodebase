﻿---@class LeanPool
---@field public HelpUrlPrefix string
---@field public ComponentPathPrefix string
---@field public Links Dictionary`2
local LeanPool={ }
---@public
---@param prefab GameObject
---@param parent Transform
---@param worldPositionStays boolean
---@return GameObject
function LeanPool.Spawn(prefab, parent, worldPositionStays) end
---@public
---@param prefab GameObject
---@param position Vector3
---@param rotation Quaternion
---@param parent Transform
---@param worldPositionStays boolean
---@return GameObject
function LeanPool.Spawn(prefab, position, rotation, parent, worldPositionStays) end
---@public
---@return void
function LeanPool.DespawnAll() end
---@public
---@param clone Component
---@param delay Single
---@return void
function LeanPool.Despawn(clone, delay) end
---@public
---@param clone GameObject
---@param delay Single
---@return void
function LeanPool.Despawn(clone, delay) end
Lean.Pool.LeanPool = LeanPool