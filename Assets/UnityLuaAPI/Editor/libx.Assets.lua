﻿---@class Assets : MonoBehaviour
---@field public ManifestAsset string
---@field public Extension string
---@field public runtimeMode boolean
---@field public loadDelegate Func`3
---@field public basePath string
---@field public updatePath string
local Assets={ }
---@public
---@return String[]
function Assets.GetAllAssetPaths() end
---@public
---@param path string
---@return void
function Assets.AddSearchPath(path) end
---@public
---@return ManifestRequest
function Assets.Initialize() end
---@public
---@return void
function Assets.Clear() end
---@public
---@param path string
---@param additive boolean
---@return SceneAssetRequest
function Assets.LoadSceneAsync(path, additive) end
---@public
---@param scene SceneAssetRequest
---@return void
function Assets.UnloadScene(scene) end
---@public
---@param path string
---@param type Type
---@return AssetRequest
function Assets.LoadAssetAsync(path, type) end
---@public
---@param path string
---@param type Type
---@return AssetRequest
function Assets.LoadAsset(path, type) end
---@public
---@param asset AssetRequest
---@return void
function Assets.UnloadAsset(asset) end
libx.Assets = Assets